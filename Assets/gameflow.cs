﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameflow : MonoBehaviour
{
    // Start is called before the first frame update
    
    public Transform tile1Obj;
    private Vector3 nextTileSpawn;
    public Transform stonesObj;
    private Vector3 nextStoneSpawn;
    private int randX;
    public Transform crateObj;
    private Vector3 nextCrateSpawn;
    public Transform barrelObj;
    private Vector3 nextBarrelSpawn;
    public Transform cartObj;
    private Vector3 nextCartSpawn;
    private int randChoice;
    public static int totalCoins = 0;
    public Transform coinObj;
   
    
    void Start()
    {
        nextTileSpawn.z = 24;
        StartCoroutine(spawnTile());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawnTile()
    {
        yield return new WaitForSeconds(1);
        randX = Random.Range(-1, 2);
        nextStoneSpawn = nextTileSpawn;
        nextStoneSpawn.y = .2f;
        nextStoneSpawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(stonesObj, nextStoneSpawn, stonesObj.rotation);


        nextTileSpawn.z += 4;
        randX = Random.Range(-1, 2);
        nextCrateSpawn.z = nextTileSpawn.z;
        nextCrateSpawn.y = .53f;
        nextCrateSpawn.x = randX;
        Instantiate(tile1Obj, nextTileSpawn, tile1Obj.rotation);
        Instantiate(crateObj, nextCrateSpawn, crateObj.rotation);

        if (randX==0)
        {
            randX = 1;
        }
        else
        if(randX==1)
        {
            randX = -1;
        }
        else
        {
            randX = 0;
        }

        randChoice = Random.Range(0, 3);
        if (randChoice == 0)
        {
            nextBarrelSpawn.z = nextTileSpawn.z;
            nextBarrelSpawn.y = .7f;
            nextBarrelSpawn.x = randX;
            Instantiate(barrelObj, nextBarrelSpawn, barrelObj.rotation);
        }

        else
        if(randChoice==1)
        {
            nextCartSpawn.z = nextTileSpawn.z;
            nextCartSpawn.y = .4f;
            nextCartSpawn.x = randX;
            Instantiate(cartObj, nextCartSpawn, cartObj.rotation);
        }
        else
        {
            nextCartSpawn.z = nextTileSpawn.z;
            nextCartSpawn.y = .56f;
            nextCartSpawn.x = randX;
            Instantiate(coinObj, nextCartSpawn, coinObj.rotation);
        }

        nextTileSpawn.z += 4;
        StartCoroutine(spawnTile());
    }
}
